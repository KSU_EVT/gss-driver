{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          opencvGtk = pkgs.opencv.override (old: { enableGtk2 = true; }); 
        in
        rec {
          packages.gss-driver = (with pkgs; stdenv.mkDerivation {
            pname = "gss-driver";
            version = "0.0.1";
            src = self;
            nativeBuildInputs = [
              cmake
              gcc
              pkgconfig
            ];
            buildInputs = [
              opencvGtk
              gtk2
            ];
            buildPhase = "make -j $(nproc)";
            installPhase = ''
              mkdir -p $out/bin
              cp GroundSpeedSensor $out/bin
            '';
          });
          defaultPackage = packages.gss-driver;

          devShell = pkgs.mkShell {
            buildInputs = [
              # extra buildInputs here

            ] ++ packages.gss-driver.buildInputs;
            nativeBuildInputs = [
              # extra nativeBuildInputs here

            ] ++ packages.gss-driver.nativeBuildInputs;
          };
        }
      );
}
