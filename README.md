## Development

#### Enter nix shell:  
`nix develop -c $SHELL`  
(`-c $SHELL` if you want to use your own shell instead of bash)

#### Configure:  
`mkdir build/`  
`cd build/`  
`cmake ..`  

#### Compile:  
`cd build/`  
`make -j$(nproc)`  
(`$(nproc)` just returns the number cores you have)

#### Run:  
`cd build/`  
`./GroundSpeedSensor`  
